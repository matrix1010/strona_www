function load(){
	validation();
	$("#add_user").submit(function(){
		
		return false;
	});
	$("#female").click(function(){
		show();
	});
	$("#male").click(function(){
		hide();
	});
}


function validation(){
	$(document).on('change', 'input', function () {
		reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		reg_kod = /^[0-9]{2}-[0-9]{3}$/;

		
		if($(this).attr("name") == "imie" && $(this).val().length < 1){
			pom = 1;
			$("#wynik").html("Imie nie jest wpisane<br />");
		}
		else if($(this).attr("name") == "nazwisko" && $(this).val().length < 1){
			pom = 1;
			$("#wynik").html("Nazwisko nie jest wpisane<br />");
		}
		else if($(this).attr("name") == "gender" && !($(this).is(':checked'))){
			pom = 1;
			$("#wynik").html("Zaznacz płeć<br />");
		}
		else if($(this).attr("name") == "email" && !(reg_email.test($(this).val()))){
			pom = 1;
			$("#wynik").html("Podany mail jest nieprawidłowy<br />");
		}
		else if($(this).attr("name") == "ulica" && $(this).val().length < 1){
			pom = 1;
			$("#wynik").html("Ulica nie jest wpisana<br />");
		}
		else if($(this).attr("name") == "miasto" && $(this).val().length < 1){
			pom = 1;
			$("#wynik").html("Miasto nie jest wpisane<br />");
		}
		//console.log(reg_kod.test($(this).val()));
		else if($(this).attr("name") == "kod_pocztowy" && !(reg_kod.test($(this).val()))){
			pom = 1;
			$("#wynik").html("Podany kod pocztowy jest nieprawidłowy<br />");
		}

		else{
			$("#wynik").html("");
		}
		
	});

	
	
}


function show(){
	$("#elem_n").slideDown();
}
function hide(){
	$("#elem_n").slideUp();
}



















$(window).on("load", function(){
	load();
});